
const express = require("express");
const path = require("path");
const fs=require('fs');
const app = express();
const data=fs.readFileSync(`${__dirname}/UserApi/userapi.json`,"utf-8");

app.get("/api/getData",(req,res)=>{
      
            console.log(data);
            res.writeHead(200,{"content-type":"application/json"});
            res.end(data);
        }
        );       

// var cors = require('cors');
// app.use(cors({origin: '*'}));
const PORT = process.env.PORT ||8000

app.use(express.json());


app.listen(PORT, (req,res) => {
    
    console.log(`app is listening to PORT ${PORT}`);
});
