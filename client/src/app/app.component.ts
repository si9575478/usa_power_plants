import { Component, OnInit } from '@angular/core';
import { AppServiceService } from './app-service.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'client';
 
  
  constructor(private service:AppServiceService){

  }
  ngOnInit() {
    this.getDataFromApi();
    
  }
  getDataFromApi(){
    this.service.getData().subscribe((response)=>{
      console.log('Response from api is',response);      

    },(err)=>{
      console.log('Response from api is',err); 
    })
  }
}
